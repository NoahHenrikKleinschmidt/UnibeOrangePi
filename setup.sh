#!/bin/bash

# This script sets up the OrangePi ubuntu systems

basedir=$(pwd)

# install Anaconda
if [ ! -d anaconda ]; then
  mkdir anaconda
  cd anaconda
  wget https://repo.anaconda.com/archive/Anaconda3-2022.05-Linux-aarch64.sh
  bash *.sh
  cd -
fi

mkdir ~/Downloads
cd ~/Downloads

# get the WhiteSur theme
wget https://github.com/vinceliuice/WhiteSur-gtk-theme/archive/refs/tags/2022-02-21.zip
unzip 2022-02-21.zip -d whitesur
cd whitesur

# install whitesur theme
icon="ubuntu"
theme_color="blue"
./install.sh -i $icon -t $theme_color

cd -

# install Whitesur icon theme
wget https://github.com/vinceliuice/WhiteSur-icon-theme/archive/refs/tags/2022-05-11.zip
unzip 2022-05-11.zip -d icons
cd icons
./install.sh

cd -

# install gnome tweak tool
sudo apt install gnome-tweak-tool
sudo add-apt-repository universe
sudo apt install gnome-shell

# install dash to dock
git clone https://github.com/micheleg/dash-to-dock.git
cd dash-to-dock/
make
make install

cd -
cd whitesur

# also install the firefox theme
./tweaks.sh -f

# Reload Gnome Shell
gnome-shell --replace

cd -

# install terminator terminal
sudo apt-get install terminator

# install vscode
cd $basedir
cd vscode
sudo apt install *.deb

cd -

# install VNC server
cd VNC*
sudo ./vncinstall
cd -

# finally we set the wallpaper to match the whitesur theme color
gsettings set org.gnome.desktop.background picture-uri file:///${basedir}/backgrounds/${theme_color}.png

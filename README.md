# UnibeOrangePi

This repository stores the setup script for the OrangePi Ubuntu systems used for Science Night 2022.

To use the setup script, simply clone this repository and run the `setup.sh` script. It will

- install Anaconda
- install VSCode
- install gnome + tweaks
- install VNC Server (RealVNC)
- install WhiteSur + Icons theme
- install dash-to-docks